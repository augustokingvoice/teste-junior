# consumir

Neste teste deve-se desenvolver um script com o objetivo de:
*  consumir a API do github;
*  receber como parametro, o nome de um usuário que será usado como filtro na pesquisa do github;
*  salvar em um arquivo (.txt, .csv, .json) o nome do usuário, e o nome dos repositórios que ele possui.

O script pode ser feito em: PHP ou Python.

Comitar ao finalizar o teste: o script e o arquivo com o resultado do script.

Ajuda:
*  documentação oficial da API: https://developer.github.com/v3/
*  https://developer.github.com/v3/guides/getting-started/