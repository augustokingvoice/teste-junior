# Teste Júnior

Este repositório é para ser aplicado como avaliação prática da vaga de Analista Desenvolvedor Júnior.  
Existem duas pastas "**consumir**" e "**organizar**" na raiz deste repositório, em cada uma delas temos o arquivo "**README.md**", o teste consiste em desenvolver o que é proposto nos arquivos "**README.md**".  
Fica a critério do candidato realizar a quantidade de commit necessário neste repositório.

**Importante**  
Ao iniciar este teste:
1.  clonar este repositório
2.  criar uma nova branch com seu nome ex: git checkout -b augusto
3.  publicar a branch ex: git checkout -b augusto
4.  todos os commits devem ser feitos na sua branch. 