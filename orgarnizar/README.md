# organizar

Neste teste temos um arquivo .csv, com exemplo de conteudo abaixo:
```
id,eventtype,eventtime,cid_name,cid_num,exten,uniqueid,linkedid
434139,CHAN_START,2020-02-03 08:12:43,,+551432694343,1433029900,1580728363.23088,1580728363.23088
434140,ANSWER,2020-02-03 08:12:49,1432694343,1432694343,s,1580728363.23088,1580728363.23088
434141,APP_START,2020-02-03 08:13:13,1432694343,1432694343,900,1580728363.23088,1580728363.23088
434142,CHAN_START,2020-02-03 08:13:13,,,31,1580728393.23089,1580728363.23088
434143,CHAN_START,2020-02-03 08:13:13,,,31,1580728393.23090,1580728363.23088
```

Deve se desenvolver um ou mais scripts para ler o arquivo .csv e gerar um arquivo .json, sendo que neste arquivo .json os registros devem estar agrupados pela coluna linkedid.
Exemplo:

```
{
    "1580728363.23088": [
        {
            "id":434139,
            "eventtype":"CHAN_START",
            "eventtime":"2020-02-03 08:12:43",
            "cid_name":"",
            "cid_num":"+551432694343",
            "exten":"1433029900",
            "uniqueid":"1580728363.23088",
            "linkedid":"1580728363.23088"
        },
        {
            "id":434140,
            "eventtype":"ANSWER",
            "eventtime":"2020-02-03 08:12:49",
            "cid_name":"1432694343",
            "cid_num":"1432694343",
            "exten":"s",
            "uniqueid":"1580728363.23088",
            "linkedid":"1580728363.23088"
        }
    ]
}
```

O script pode ser feito em: PHP ou Python.

Comitar o arquivo .json e o(s) script(s) ao finalizar o teste.
